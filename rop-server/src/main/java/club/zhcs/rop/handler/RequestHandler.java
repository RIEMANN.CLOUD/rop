package club.zhcs.rop.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.lang.Strings;

import club.zhcs.rop.core.ROPConsts;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface RequestHandler {

    default void handle(HttpServletRequest request, HttpServletResponse response, String gateWay)
            throws ServletException, IOException {
        request.getRequestDispatcher(Strings.isBlank(gateWay) ? request.getHeader(ROPConsts.METHOD_KEY) : gateWay)
               .forward(request, response);
    }

}
