package club.zhcs.rop.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.json.Json;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;

import club.zhcs.rop.checker.ServerSignatureVerifier;
import club.zhcs.rop.core.ROPConsts;
import club.zhcs.rop.core.ROPResponse;
import club.zhcs.rop.handler.RequestHandler;

/**
 *
 * @author 王贵源(wangguiyuan@chinarecrm.com.cn)
 */
public class ROPServlet extends HttpServlet {

    static final Log log = Logs.get();

    final RequestHandler requestHandler;

    final boolean aopCheck;

    final ServerSignatureVerifier serverSignatureVerifier;

    /**
     * 
     * @param requestHandler
     *            请求处理器
     * @param aopCheck
     *            是否aop检查
     * @param serverSignatureVerifier
     *            签名器
     */
    public ROPServlet(RequestHandler requestHandler, boolean aopCheck, ServerSignatureVerifier serverSignatureVerifier) {
        super();
        this.requestHandler = requestHandler;
        this.aopCheck = aopCheck;
        this.serverSignatureVerifier = serverSignatureVerifier;
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Date addSeconds(Date base, long seconds) {
        return Times.D(base.getTime() + seconds * 1000);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.
     * HttpServletRequest , javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void service(final HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /**
         * 1. 时间戳校验 <br>
         * 2.签名校验转移到spring的aop中去处理,这样便于获取ioc中的对象 <br>
         * 3.方法校验
         *
         */
        try {
            String method = request.getHeader(ROPConsts.METHOD_KEY);
            if (Strings.isBlank(method)) {// 空方法
                response.getWriter().write(Json.toJson(ROPResponse.exception("null method")));
                return;
            }
            String timeStemp = request.getHeader(ROPConsts.TS_KEY);
            if (Strings.isBlank(timeStemp)) {
                response.getWriter().write(Json.toJson(ROPResponse.exception("no timeStemp")));
                return;
            }
            long time = Long.parseLong(timeStemp);
            if (addSeconds(Times.D(time), Long.parseLong(getInitParameter("timeout"))).before(Times.now())) {
                response.getWriter().write(Json.toJson(ROPResponse.exception("request timeout")));
                return;
            }
            // 如果走aop检查就让aop去处理,不然在此处就需要检查签名信息
            if (aopCheck || serverSignatureVerifier.verification(request)) {
                requestHandler.handle(request, response, getInitParameter("gateWayUri"));
            }
        }
        catch (Exception e) {
            response.getWriter().write(Json.toJson(ROPResponse.exception(e)));
        }
    }

}
