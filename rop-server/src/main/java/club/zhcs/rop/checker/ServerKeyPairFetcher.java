package club.zhcs.rop.checker;

import club.zhcs.rop.core.signer.KeyPairFetcher;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface ServerKeyPairFetcher extends KeyPairFetcher {

}
