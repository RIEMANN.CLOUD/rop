package club.zhcs.rop.checker;

import javax.servlet.http.HttpServletRequest;

import org.nutz.lang.Strings;
import org.nutz.lang.Times;

import club.zhcs.rop.core.ROPConsts;
import club.zhcs.rop.core.ROPException;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface RequestChecker {

    default boolean check(HttpServletRequest request, long timeout) {
        // 方法检查
        String method = request.getHeader(ROPConsts.METHOD_KEY);
        if (Strings.isBlank(method)) {// 空方法
            throw ROPException.exception("null method");
        }

        // 时间戳检查
        String timeStemp = request.getHeader(ROPConsts.TS_KEY);
        if (Strings.isBlank(timeStemp)) {
            throw ROPException.exception("no timeStemp");
        }

        if (Times.D(Long.parseLong(timeStemp) + timeout * 1000).before(Times.now())) {
            throw ROPException.exception("request timeout");
        }

        return true;
    }
}
