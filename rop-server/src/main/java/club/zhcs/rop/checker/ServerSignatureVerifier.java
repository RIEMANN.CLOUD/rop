package club.zhcs.rop.checker;

import javax.servlet.http.HttpServletRequest;

import club.zhcs.rop.core.signer.Signer;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface ServerSignatureVerifier extends Signer {

    /**
     * 验证请求
     * 
     * @param request
     *            请求
     * @return 是否通过验证
     */
    public boolean verification(HttpServletRequest request);
}
