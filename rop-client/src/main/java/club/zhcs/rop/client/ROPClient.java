package club.zhcs.rop.client;

import java.net.Proxy;
import java.util.stream.Collectors;

import org.nutz.http.Header;
import org.nutz.http.Request;
import org.nutz.http.Response;
import org.nutz.http.Sender;
import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.lang.Times;
import org.nutz.lang.random.R;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;

import club.zhcs.rop.core.ROPConsts;
import lombok.Data;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Data
public class ROPClient {

    public static ROPClient create(String endpoint, ClientSigner signer) {
        ROPClient client = new ROPClient();
        client.setEndpoint(endpoint);
        client.setSigner(signer);
        return client;
    }

    public static ROPClient create(
                                   String endpoint,
                                   ClientSigner signer,
                                   boolean enableResponseCheck) {
        ROPClient client = new ROPClient();
        client.setEndpoint(endpoint);
        client.setSigner(signer);
        client.setEnableResponseCheck(enableResponseCheck);
        return client;
    }

    private String endpoint;// 调用点
    private boolean enableResponseCheck = true;
    Proxy proxy;
    ClientSigner signer;
    Log log = Logs.get();

    private ROPClient() {}

    public Response send(String endpoint, String publicKey, ROPRequest request) {
        setEndpoint(endpoint);
        return send(publicKey, request);
    }

    public Response send(String publicKey, ROPRequest request) {
        Response response;
        if (proxy != null) {
            response = Sender.create(toRequest(publicKey, request)).setProxy(proxy).send();
        } else {
            response = Sender.create(toRequest(publicKey, request)).send();
        }
        if (!response.isOK()) {
            throw Lang.makeThrow("请求失败,状态码:%d", response.getStatus());
        }
        if (log.isDebugEnabled()) {
            Header header = response.getHeader();
            log.debugf("response headers -> %s",
                       Json.toJson(header.getAll()
                                         .stream()
                                         .map(item -> NutMap.NEW().addv("key", item.getKey()).addv("value", item.getValue()))
                                         .collect(Collectors.toList())));
        }

        if (!isEnableResponseCheck()
            || signer.verification(response,
                                   publicKey,
                                   request.getHeader().get(ROPConsts.NONCE_KEY),
                                   request.getGateway())) {
            return response;
        }
        throw Lang.makeThrow("响应签名检查失败!");
    }

    /**
     * 处理header
     *
     * @param request
     * @return
     */
    private Header signHeader(String publicKey, ROPRequest request) {
        String nonce = R.UU16();
        String ts = Times.now().getTime() + "";
        Header header = request.getHeader()
                               .set(ROPConsts.PUBLIC_KEY_KEY, publicKey)
                               .set(ROPConsts.METHOD_KEY, request.getGateway())
                               .set(ROPConsts.NONCE_KEY, nonce)
                               .set(ROPConsts.TS_KEY, ts)
                               .set(ROPConsts.SIGN_KEY,
                                    signer.sign(signer.fetcher().fetch(publicKey), ts, request.getGateway(), nonce, request));
        return request.getData() == null || request.getData().length == 0 ? header.asFormContentType()
                                                                          : header.asJsonContentType();
    }

    public Request toRequest(String publicKey, ROPRequest request) {
        Request req = Request.create(endpoint, request.getMethod());
        req.setParams(request.getParams());
        req.setData(request.getData());
        req.setHeader(signHeader(publicKey, request));
        Header header = req.getHeader();
        log.debugf("send headers %s", header);
        return req;
    }
}
