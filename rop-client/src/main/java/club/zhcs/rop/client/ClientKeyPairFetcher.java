package club.zhcs.rop.client;

import club.zhcs.rop.core.signer.KeyPairFetcher;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface ClientKeyPairFetcher extends KeyPairFetcher {

}
