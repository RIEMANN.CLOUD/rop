package club.zhcs.rop.client;

import org.nutz.http.Response;

import club.zhcs.rop.core.signer.Signer;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface ClientSigner extends Signer {
    /**
     * 客户端签名
     *
     * @param privateKey
     *            应用密钥
     * @param timestamp
     *            时间戳
     * @param gateway
     *            方法/路由
     * @param nonce
     *            随机串
     * @param request
     *            请求
     * @return 签名字符串
     */
    public String sign(String privateKey, String timestamp, String gateway, String nonce, ROPRequest request);

    /**
     * 验证响应签名
     * 
     * @param response
     * @param publicKey
     * @param string
     * @param gateway
     * @return
     */
    public boolean verification(Response response, String publicKey, String string, String gateway);
}
