package club.zhcs.rop.client;

import java.io.IOException;
import java.io.InputStreamReader;

import org.nutz.http.Response;
import org.nutz.lang.Lang;
import org.nutz.lang.Streams;

import club.zhcs.rop.core.ROPConsts;
import club.zhcs.rop.core.signer.KeyPairFetcher;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public class ROPClientRSASigner implements ClientSigner {

    ClientKeyPairFetcher fetcher;

    /**
     * @param fetcher
     */
    public ROPClientRSASigner(ClientKeyPairFetcher fetcher) {
        super();
        this.fetcher = fetcher;
    }

    protected String getDataMate(ROPRequest request) {
        if (request.isGet()) {
            // 参数顺序问题
            String query = request.getUrlEncodedParams();
            return queryMate(query);
        }
        StringBuilder info;
        try {
            info = Streams.read(new InputStreamReader(request.getInputStream()));
        }
        catch (IOException e) {
            throw Lang.wrapThrow(e);
        }
        return Lang.md5(info);
    }

    @Override
    public String sign(String privateKey, String timestamp, String gateway, String nonce, ROPRequest request) {
        return sign(privateKey, timestamp, gateway, nonce, getDataMate(request));
    }

    @Override
    public boolean verification(Response response, String publicKey, String string, String gateway) {
        String timestamp = response.getHeader().get(ROPConsts.TS_KEY);
        String sign = response.getHeader().get(ROPConsts.SIGN_KEY);
        String nonce = response.getHeader().get(ROPConsts.NONCE_KEY);
        return verification(publicKey, timestamp, gateway, nonce, Lang.md5(response.getContent()), sign);
    }

    /**
     * @return
     * @see club.zhcs.rop.core.signer.Signer#fetcher()
     */
    @Override
    public KeyPairFetcher fetcher() {
        return fetcher;
    }

}
