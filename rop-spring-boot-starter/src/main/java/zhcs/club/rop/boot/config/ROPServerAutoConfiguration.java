package zhcs.club.rop.boot.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServlet;

import org.nutz.lang.Strings;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;

import club.zhcs.rop.checker.ROPServerRSASignatureVerifier;
import club.zhcs.rop.checker.RequestChecker;
import club.zhcs.rop.checker.ServerKeyPairFetcher;
import club.zhcs.rop.checker.ServerSignatureVerifier;
import club.zhcs.rop.handler.RequestHandler;
import club.zhcs.rop.servlet.ROPServlet;
import zhcs.club.rop.boot.aop.ROPSignInterceptor;
import zhcs.club.rop.boot.handler.ROPExceptionHandler;
import zhcs.club.rop.boot.handler.ROPResponseBodyAdvice;
import zhcs.club.rop.boot.server.ResettableStreamHttpServletRequestFilter;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Configuration
@EnableConfigurationProperties(ROPServerConfigurationProperties.class)
@ConditionalOnExpression("${rop.server.enabled:true}")
public class ROPServerAutoConfiguration {

    /**
     * 请求检查器
     * 
     * @return 请求检查器
     */
    @Bean
    @ConditionalOnMissingBean(RequestChecker.class)
    public RequestChecker requestChecker() {
        return new RequestChecker() {};
    }

    /**
     * 请求处理器
     * 
     * @return 请求处理器
     */
    @Bean
    @ConditionalOnMissingBean(RequestHandler.class)
    public RequestHandler requestHandler() {
        return new RequestHandler() {};
    }

    @Bean
    @ConditionalOnMissingBean(ServerKeyPairFetcher.class)
    public ServerKeyPairFetcher serverKeyPairFetcher(ROPServerConfigurationProperties config) {
        return new ServerKeyPairFetcher() {

            @Override
            public String fetch(String publicKey) {
                return config.getKeyPairs()
                             .stream()
                             .filter(item -> Strings.equals(item.getPublicKey(), publicKey))
                             .findAny()
                             .orElseThrow(NullPointerException::new)
                             .getPrivateKey();
            }
        };
    }

    /**
     * 服务器端签名器
     * 
     * @param fetcher
     *            服务器端公私钥获取器
     * @return 服务器端签名器
     */
    @Bean
    public ServerSignatureVerifier serverSignatureVerifier(ServerKeyPairFetcher fetcher) {
        return new ROPServerRSASignatureVerifier(fetcher);
    }

    /**
     * ROP AOP拦截器
     * 
     * @param properties
     *            篇日志
     * @return ROP AOP拦截器
     */
    @Bean
    @ConditionalOnBean(ServerSignatureVerifier.class)
    public ROPSignInterceptor ropSignInterceptor(ROPServerConfigurationProperties properties) {
        return new ROPSignInterceptor(properties.getTimeout());
    }

    /**
     * ROP 响应处理器
     * 
     * @return ROP 响应处理器
     */
    @Bean
    @ConditionalOnMissingBean(ROPResponseBodyAdvice.class)
    @ConditionalOnBean(ServerSignatureVerifier.class)
    public ROPResponseBodyAdvice responseBodyAdvice() {
        return new ROPResponseBodyAdvice();
    }

    /**
     * Rop 异常处理器
     * 
     * @return Rop 异常处理器
     */
    @Bean
    @ConditionalOnMissingBean(ROPExceptionHandler.class)
    public ROPExceptionHandler ropExceptionHandler() {
        return new ROPExceptionHandler();
    }

    /**
     * rop servlet注册
     * 
     * @param properties
     *            配置
     * @param multipartConfigFactory
     *            文件上传
     * @param requestHandler
     *            请求处理
     * @param serverSignatureVerifier
     *            前面验证
     * @return rop servlet注册
     */
    @Bean
    public ServletRegistrationBean<HttpServlet> servletRegistrationBean(ROPServerConfigurationProperties properties,
                                                                        MultipartConfigElement multipartConfigFactory,
                                                                        RequestHandler requestHandler,
                                                                        ServerSignatureVerifier serverSignatureVerifier) {
        ServletRegistrationBean<HttpServlet> ropServletRegistrationBean = new ServletRegistrationBean<>(new ROPServlet(requestHandler,
                                                                                                                       true,
                                                                                                                       serverSignatureVerifier));
        ropServletRegistrationBean.addUrlMappings(properties.getRopPath());
        ropServletRegistrationBean.setMultipartConfig(multipartConfigFactory);

        Map<String, String> initParameters = new HashMap<>();

        initParameters.put("timeout", "" + properties.getTimeout());
        if (Strings.isNotBlank(properties.getGateWayUri())) {
            initParameters.put("gateWayUri", properties.getGateWayUri());
        }
        ropServletRegistrationBean.setInitParameters(initParameters);
        return ropServletRegistrationBean;
    }

    /**
     * 可重读流Filter注入
     * 
     * @param properties
     *            配置
     * @param multipartResolver
     *            文件出来
     * @return 可重读流Filter注入
     */
    @Bean
    public FilterRegistrationBean<HttpFilter> filterRegistrationBean(ROPServerConfigurationProperties properties,
                                                                     MultipartResolver multipartResolver) {
        FilterRegistrationBean<HttpFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new ResettableStreamHttpServletRequestFilter(multipartResolver));
        registration.addUrlPatterns(properties.getRopPath());
        registration.setOrder(1);
        return registration;
    }
}
