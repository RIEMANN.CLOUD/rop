package zhcs.club.rop.boot.handler;

import javax.servlet.http.HttpServletResponse;

import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import club.zhcs.rop.core.ROPException;
import club.zhcs.rop.core.ROPResponse;
import zhcs.club.rop.boot.aop.RopController;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@RestController
@ControllerAdvice(annotations = RopController.class)
public class ROPExceptionHandler {

    Log log = Logs.get();

    @ExceptionHandler(value = ROPException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ROPResponse<Void> rop(HttpServletResponse response, ROPException e) {
        log.error("error=>", e);
        return ROPResponse.<Void> exception(e);
    }
}
