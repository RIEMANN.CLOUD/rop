package zhcs.club.rop.boot.config;

import java.net.Proxy.Type;

import lombok.Data;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Data
public class ProxyConfigurationProperties {

    boolean enable;

    String host;

    int port;

    Type type;
}
