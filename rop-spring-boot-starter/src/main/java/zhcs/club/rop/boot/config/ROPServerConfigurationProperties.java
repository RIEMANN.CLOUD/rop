package zhcs.club.rop.boot.config;

import java.util.List;

import org.nutz.lang.Lang;
import org.springframework.boot.context.properties.ConfigurationProperties;

import club.zhcs.rop.core.signer.RSA.KeyPair;
import lombok.Data;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Data
@ConfigurationProperties(prefix = "rop.server")
public class ROPServerConfigurationProperties {

    boolean enabled = true;

    /**
     * 自行处理的gateway
     */
    String gateWayUri;

    /**
     * 接口路径
     */
    String ropPath = "/rop.endpoint";

    /**
     * 接口超时时间
     */
    long timeout = 5;

    List<KeyPair> keyPairs = Lang.list();

}
