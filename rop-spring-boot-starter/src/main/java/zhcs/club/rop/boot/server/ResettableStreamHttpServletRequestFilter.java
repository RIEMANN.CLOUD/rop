package zhcs.club.rop.boot.server;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartResolver;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public class ResettableStreamHttpServletRequestFilter extends HttpFilter {

    transient MultipartResolver multipartResolver;

    /**
     * @param multipartResolver
     */
    public ResettableStreamHttpServletRequestFilter(MultipartResolver multipartResolver) {
        super();
        this.multipartResolver = multipartResolver;
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void destroy() {
        // 兼容低版本
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 兼容低版本
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        ServletRequest requestWrapper = null;
        if (request instanceof HttpServletRequest) {
            requestWrapper = new ResettableStreamHttpServletRequest((HttpServletRequest) request);
        }
        if (requestWrapper == null || multipartResolver.isMultipart((HttpServletRequest) request)) {
            chain.doFilter(request, response);
        } else {
            chain.doFilter(requestWrapper, response);
        }
    }

}
