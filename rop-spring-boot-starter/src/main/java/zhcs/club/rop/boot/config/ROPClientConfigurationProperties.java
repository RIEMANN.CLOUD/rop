package zhcs.club.rop.boot.config;

import java.util.List;

import org.nutz.lang.Lang;
import org.springframework.boot.context.properties.ConfigurationProperties;

import club.zhcs.rop.core.signer.RSA.KeyPair;
import lombok.Data;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Data
@ConfigurationProperties(prefix = "rop.client")
public class ROPClientConfigurationProperties {

    boolean enabled = false;

    String endpoint;

    boolean enableResponseCheck = true;

    ProxyConfigurationProperties proxy = new ProxyConfigurationProperties();

    List<KeyPair> keyPairs = Lang.list();

}
