package zhcs.club.rop.boot.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

import club.zhcs.rop.checker.RequestChecker;
import club.zhcs.rop.checker.ServerSignatureVerifier;
import club.zhcs.rop.core.ROPException;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Aspect
public class ROPSignInterceptor {

    @Autowired
    RequestChecker checker;

    @Autowired
    HttpServletRequest request;

    @Autowired
    HttpServletResponse response;

    @Autowired
    ServerSignatureVerifier verifier;

    long timeout;

    public ROPSignInterceptor(long timeout) {
        this.timeout = timeout;
    }

    @Around("@within(zhcs.club.rop.boot.aop.RopController)|| @annotation(zhcs.club.rop.boot.aop.RopController)")
    public Object filter(ProceedingJoinPoint point) throws Throwable {
        if (checker.check(request, timeout) && verifier.verification(request)) {
            return point.proceed();
        } else {
            throw new ROPException("checkSign failed");
        }
    }

}
