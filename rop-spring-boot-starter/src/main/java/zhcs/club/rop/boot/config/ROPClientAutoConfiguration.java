package zhcs.club.rop.boot.config;

import java.net.InetSocketAddress;
import java.net.Proxy;

import org.nutz.lang.Strings;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import club.zhcs.rop.client.ClientKeyPairFetcher;
import club.zhcs.rop.client.ROPClient;
import club.zhcs.rop.client.ROPClientRSASigner;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Configuration
@EnableConfigurationProperties(ROPClientConfigurationProperties.class)
@ConditionalOnExpression("${rop.client.enabled:false}")
public class ROPClientAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(ClientKeyPairFetcher.class)
    public ClientKeyPairFetcher clientKeyPairFetcher(ROPClientConfigurationProperties config) {
        return new ClientKeyPairFetcher() {

            @Override
            public String fetch(String publicKey) {
                return config.getKeyPairs()
                             .stream()
                             .filter(item -> Strings.equals(item.getPublicKey(), publicKey))
                             .findAny()
                             .orElseThrow(NullPointerException::new)
                             .getPrivateKey();
            }
        };
    }

    @Bean
    public ROPClientRSASigner ropClientRSASigner(ClientKeyPairFetcher fetcher) {
        return new ROPClientRSASigner(fetcher);
    }

    @Bean
    public ROPClient ropClient(ROPClientConfigurationProperties configProperties, ROPClientRSASigner signer) {
        ROPClient client = ROPClient.create(
                                            configProperties.getEndpoint(),
                                            signer,
                                            configProperties.isEnableResponseCheck());
        if (configProperties.getProxy() != null && configProperties.getProxy().isEnable()) {
            Proxy proxy = new Proxy(configProperties.getProxy().getType(),
                                    new InetSocketAddress(configProperties.getProxy().getHost(), configProperties.getProxy().getPort()));
            client.setProxy(proxy);
        }
        return client;
    }
}
