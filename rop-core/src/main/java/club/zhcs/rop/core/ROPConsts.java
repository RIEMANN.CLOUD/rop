package club.zhcs.rop.core;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public class ROPConsts {

    public static final String PUBLIC_KEY_KEY = "rop-sign-appkey";
    public static final String PRIVATE_KEY_KEY = "rop-sign-appsecret";
    public static final String METHOD_KEY = "rop-service-method";
    public static final String NONCE_KEY = "rop-nonce";
    public static final String PARAS_KEY = "rop-paras";
    public static final String SIGNER_KEY = "rop-signer-name";
    public static final String SIGN_KEY = "rop-sign";
    public static final String TS_KEY = "rop-ts";

    private ROPConsts() {}
}
