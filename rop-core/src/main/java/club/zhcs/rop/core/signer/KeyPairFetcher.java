package club.zhcs.rop.core.signer;

/**
 * 
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public interface KeyPairFetcher {

    public String fetch(String publicKey);
}
