package club.zhcs.rop.core;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.nutz.lang.util.NutMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ROPResponse<T> {
    /**
     * 
     * @author Kerbores(kerbores@gmail.com)
     *
     */
    public enum OperationState {
        /**
         * 成功
         */
        SUCCESS,
        /**
         * 失败
         */
        FAIL,
        /**
         * 异常
         */
        EXCEPTION;

    }

    @Default
    private NutMap ext = new NutMap();

    @Default
    private OperationState state = OperationState.SUCCESS;

    private String[] errors;

    private T data;

    /**
     * 返回成功
     * 
     * @param <T>
     *            类型
     * @return 成功的响应
     */
    public static <T> ROPResponse<T> success() {
        return ROPResponse.<T> builder()
                          .build();
    }

    /**
     * 携带数据返回成功
     * 
     * @param <T>
     *            数据泛型
     * @param t
     *            数据
     * @return 成功的结果
     */
    public static <T> ROPResponse<T> success(T t) {
        return ROPResponse.<T> builder()
                          .data(t)
                          .build();
    }

    /**
     * 返回失败及原因
     * 
     * @param <T>
     *            类型
     * @param msgs
     *            失败原因
     * @return 失败的结果
     */
    public static <T> ROPResponse<T> fail(String... msgs) {
        return ROPResponse.<T> builder()
                          .state(OperationState.FAIL)
                          .errors(msgs)
                          .build();
    }

    /**
     * 返回异常及原因
     * 
     * @param <T>
     *            类型
     * @param msgs
     *            异常原因
     * @return 异常的结果
     */
    public static <T> ROPResponse<T> exception(String... msgs) {
        return ROPResponse.<T> builder()
                          .state(OperationState.EXCEPTION)
                          .errors(msgs)
                          .build();
    }

    /**
     * 返回异常及原因
     * 
     * @param <T>
     *            类型
     * @param msgs
     *            异常原因
     * @return 异常的结果
     */
    public static <T> ROPResponse<T> exception(List<String> msgs) {
        return ROPResponse.<T> builder()
                          .state(OperationState.EXCEPTION)
                          .errors(msgs.toArray(new String[msgs.size()]))
                          .build();
    }

    /**
     * 返回异常及原因
     * 
     * @param <T>
     *            类型
     * @param throwables
     *            异常
     * @return 异常的结果
     */
    public static <T> ROPResponse<T> exception(Throwable... throwables) {
        return exception(Arrays.stream(throwables).map(Throwable::getMessage).collect(Collectors.toList()));
    }

    /**
     * 添加扩展数据
     * 
     * @param key
     *            数据key
     * @param value
     *            数据
     * @return 返回结果对象
     */
    public ROPResponse<T> addExtData(String key, Object value) {
        getExt().setv(key, value);
        return this;
    }

    /**
     * 是否成功
     * 
     * @return 是否成功标识
     */
    public boolean isSuccess() {
        return getState() == OperationState.SUCCESS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return Json.toJson(this, JsonFormat.forLook());
    }

}
