package club.zhcs.rop.core.signer;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.log.Logs;

/**
 * @author 王贵源(wangguiyuan@chinarecrm.com.cn)
 */
public interface Signer {

    /**
     * 签名验证
     * 
     * @param publicKey
     *            公钥
     * @param timestamp
     *            时间戳
     * @param gateway
     *            网关/方法名称
     * @param nonce
     *            随机串
     * @param dataMate
     *            数据元数据
     * @param sign
     *            签名字符串
     * @return 是否匹配
     */
    public default boolean verification(String publicKey,
                                        String timestamp,
                                        String gateway,
                                        String nonce,
                                        String dataMate,
                                        String sign) {
        String[] temp = Lang.array(timestamp, gateway, nonce, dataMate);
        Arrays.sort(temp);
        Logs.get().debugf("sign with RSA args %s", Json.toJson(temp));
        return RSA.verify(Strings.join("", temp), sign, publicKey);
    }

    /**
     * 签名(私钥签名,公钥验证)
     * 
     * @param privateKey
     *            私钥
     * @param timestamp
     *            时间戳
     * @param gateway
     *            网关/方法名称
     * @param nonce
     *            随机串
     * @param dataMate
     *            数据元数据
     * @return 签名字符串
     */
    public default String sign(String privateKey, String timestamp, String gateway, String nonce, String dataMate) {
        String[] temp = Lang.array(timestamp, gateway, nonce, dataMate);
        Arrays.sort(temp);
        Logs.get().debugf("sign with RSA args %s", Json.toJson(temp));
        return RSA.sign(Strings.join("", temp), privateKey);
    }

    /**
     * 根据appSecret获取器签名
     *
     * @param publicKey
     *            公钥
     * @param timestamp
     *            时间戳
     * @param gateway
     *            方法/路由
     * @param nonce
     *            随机串
     * @param dataMate
     *            数据元数据
     * @return 签名字符串
     */
    public default String signByPublicKey(String publicKey,
                                          String timestamp,
                                          String gateway,
                                          String nonce,
                                          String dataMate) {
        return sign(fetcher().fetch(publicKey), timestamp, gateway, nonce, dataMate);
    }

    public KeyPairFetcher fetcher();

    public default String queryMate(String query) {
        Map<String, String[]> queryMap = toQueryMap(query);
        StringBuilder sb = new StringBuilder();
        queryMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(paramEntry -> {
            String paramValue = String.join(",",
                                            Arrays.stream(paramEntry.getValue()).sorted().toArray(String[]::new));
            sb.append(paramEntry.getKey()).append("=").append(paramValue).append('#');
        });
        return Lang.md5(sb.toString());
    }

    /**
     * @param query
     * @return
     */
    public default Map<String, String[]> toQueryMap(String query) {
        Map<String, String[]> target = new LinkedHashMap<>();
        if (Strings.isBlank(query)) {
            return target;
        }
        Arrays.stream(query.split("&")).forEach(part -> {
            String[] parts = part.split("=");
            target.put(parts[0], parts[1].split(","));
        });
        return target;
    }

}
