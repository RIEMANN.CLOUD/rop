package club.zhcs.rop.core;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
public class ROPException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ROPException() {}

    public ROPException(String msg) {
        super(msg);
    }

    public static ROPException exception(String msg) {
        return new ROPException(msg);
    }

}
