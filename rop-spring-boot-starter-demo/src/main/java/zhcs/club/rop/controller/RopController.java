package zhcs.club.rop.controller;

import org.nutz.http.Request.METHOD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import club.zhcs.rop.client.ROPClient;
import club.zhcs.rop.client.ROPRequest;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@RestController
public class RopController {

    @Autowired
    ROPClient client;

    @GetMapping("rop")
    public String rop() {
        return client.send("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh8THxRYSowwbzLG2YnhAaAhj6gywBlQDOJf716uOf84kNTL5CQzMHvXtBwBZFtH3wZ8i/GbnUzuACppSkOYsrPfExRM7U8qFCPstX76Qsn2U6TRAlurZn6Hq/nARbqQMwKuic606oySxeQPnZzTopxywnawjw9I3a3V8x1ID+Cv2Y+4LdT3VXPgCIOLIJSNcvbPSCSoXoDFe9iRC+DlfHRzyhcY/uVpoKE/agvAX3jT4ZYsHeir6VgkkMa3kGTFqnwP1yhjuoIwJDtjZ+aIaMs01K9+NNUFfWNytZb7EWFTkvTBQH62+iezaq0jfpeu6GI0+VoGoJ6xmHkRKq9xuvQIDAQAB",
                           ROPRequest.create("/test", METHOD.GET))
                     .getContent();
    }

}
