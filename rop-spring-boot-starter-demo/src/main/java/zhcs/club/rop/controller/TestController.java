package zhcs.club.rop.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import zhcs.club.rop.boot.aop.RopController;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@RestController
@RopController
public class TestController {

    @GetMapping("test")
    public String test() {
        return "ok";
    }

}
