package zhcs.club.rop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Kerbores(kerbores@gmail.com)
 *
 */
@SpringBootApplication
public class ROPDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ROPDemoApplication.class, args);
    }

}
